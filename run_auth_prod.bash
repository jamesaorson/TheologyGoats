#! /bin/bash

cd /home/ubuntu/TheologyGoats

export ENV=production
export FLASK_APP=auth
export FLASK_ENV=production

pip3 install -e .

gunicorn --timeout 300 --workers=3 --worker-class gevent auth:app --bind 0.0.0.0:5001